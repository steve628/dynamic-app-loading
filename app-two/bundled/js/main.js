const lionImagePath = 'https://denver.cbslocal.com/wp-content/uploads/sites/15909806/2020/06/CUBS_DENVERZOO_06.jpg';
const kittenImagePath = 'https://media.os.fressnapf.com/cms/2020/08/5f2cfc5f7f982-5f2cfc5f7f983Ratgeber-Katze_Outdoor_Kitten_1200x527.jpg.jpg?t=cmsimg_540';

function changeImage() {
    const image = document.getElementById('mainImage');
    const lion = document.getElementById('lionRadio');
    image.src = lion.checked ? lionImagePath : kittenImagePath;
}

function onKeyPress() {
    const input = document.getElementById('textId');
    const output = document.getElementById('outputText');
    output.innerText = input.value;
}

function onSubmit() {
    const text = document.getElementById('textId').value;
    communicationService.sendData({
        text: text,
        count: 0
    });
}

function onReset() {
    document.getElementById('outputText').innerText = null;
}

function onClick() {
    communicationService.sendData({
        text: 'I was clicked',
        count: 0
    });
}

function setInputData(data) {
    const dataInput = document.getElementById('dataInput');
    const html = `
        <label>Data from main app:</label>
        <label>Text:</label>
        <p>${String(data.text)}</p>
        <label>Count: </label>
        <p>${String(data.count)}</p>
    `;
    dataInput.innerHTML = html;
}

// add a listener with call back
communicationService.addDataListener(data => {
    if (data) {
        setInputData(data);
    }
});