var CommunicationLibrary = {
    SendData: function(data) {
        if (window.top) {
            const json = JSON.parse(Pointer_stringify(data));
            window.top.postMessage(json, window.origin);
        } else {
            throw Error('App is not embedded');
        }
    },

    AddDataListener: function() {
        window.addEventListener('message', function (message) {
            console.log('Recived message: ', message.data);
            const dataString = JSON.stringify(message.data);
            window.unityInstance.SendMessage('Management', 'ReciveData', dataString);
        }, false);
    }
};

mergeInto(LibraryManager.library, CommunicationLibrary);