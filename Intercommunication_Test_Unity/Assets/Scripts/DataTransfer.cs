using AOT;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

public class DataTransfer : MonoBehaviour
{
    [DllImport("__Internal")]
    private static extern void AddDataListener();

    [DllImport("__Internal")]
    private static extern void SendData(string data);

    public TMPro.TextMeshPro text;

    public TMPro.TMP_InputField inputField;

    // Start is called before the first frame update
    void Start()
    {
        AddDataListener();
    }

    public void ReciveData(string dataString)
    {
        Data data = JsonUtility.FromJson<Data>(dataString);
        text.text = "Recived Text:\n" + data.text + "\nRecived Number:\n" + data.count;
    }

    public void OnSendDataClick()
    {
        Data data = new Data(inputField.text);
        inputField.text = "";
        SendDataToWeb(data);
    }

    private void SendDataToWeb(Data data) 
    {
        Debug.Log("Data before serialize: " + data.ToString());
        string json = JsonUtility.ToJson(data);
        Debug.Log("Data after serialize: " + json);
        SendData(json);
    }
}
