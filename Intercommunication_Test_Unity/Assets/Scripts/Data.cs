using System;

[Serializable]
public class Data
{
    public string text;
    public double count;

    public Data() 
    {
        text = "";
        count = -1.0;
    }

    public Data(string text)
    {
        this.text = text;
        this.count = -1.0;
    }

    public Data(string text, double count)
    {
        this.text = text;
        this.count = count;
    }
}
