using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateClones : MonoBehaviour
{
    public GameObject mainObject;
    public Vector3 startPosition;
    public float spornTime;

    private GameObject[] instances;
    private Coroutine coroutine;

    public void SpornObjects(int amount)
    {
        ResetObjects();
        instances = new GameObject[amount];
        coroutine = StartCoroutine(CreateObject(amount));
    }

    public void ResetObjects()
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
            coroutine = null;
        }

        if (instances != null)
        {
            foreach (GameObject instance in instances)
            {
                Destroy(instance);
            }
            instances = null;
        }
    }

    private IEnumerator CreateObject(int amount)
    {
        Rigidbody rigidbody;
        for (int i = 0; i < amount; i++)
        {
            instances[i] = Instantiate<GameObject>(mainObject, startPosition, mainObject.transform.rotation);
            rigidbody = instances[i].GetComponent<Rigidbody>();
            rigidbody.useGravity = true;
            rigidbody.AddForce(Vector3.right * 3, ForceMode.Impulse);
            yield return new WaitForSeconds(spornTime);
        }
    }
}
