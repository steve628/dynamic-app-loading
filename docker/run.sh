#!/bin/sh

# replace ATS_BASE_HREF in index.html
INDEX_FILE=/usr/share/nginx/html/index.html
set '<base href="/" />' '<base href="/"/>' '<base href="/">'
REPLACE="<base href=\"${BASE_HREF}\" />"
ESCAPED_REPLACE=$(printf '%s\n' "$REPLACE" | sed -e 's/[\/&]/\\&/g')
for search do
    escaped_search=$(printf '%s\n' "$search" | sed -e 's/[\/&]/\\&/g')
    sed -i "s/${escaped_search}/${ESCAPED_REPLACE}/" ${INDEX_FILE}
done



nginx -g 'daemon off;'