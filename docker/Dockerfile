#build stage
FROM node:14.16.1 AS build

# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY main-app-event/package*.json /usr/angular-build/
# later be replaced by an remote package
COPY main-app-event/ngx-app-integration-0.0.1.tgz /usr/angular-build/

# install angular cli globally
RUN npm install -g @angular/cli@12.1.2

WORKDIR /usr/angular-build

# install dependencies
RUN npm install

# build in production mode
COPY main-app-event/angular.json angular.json
COPY main-app-event/tsconfig.json tsconfig.json
COPY main-app-event/tsconfig.app.json tsconfig.app.json
COPY main-app-event/src src
RUN rm src/_redirects
RUN ng build

#run stage
FROM nginx:alpine AS run

# remove default nginx index page
RUN rm -rf /usr/share/nginx/html/*

# copy configuration
COPY docker/nginx.conf /etc/nginx/nginx.conf
COPY docker/run.sh run.sh

# copy all apps
COPY apps /usr/share/nginx/html/apps
COPY app_four /usr/share/nginx/html/unity
COPY app-one-routing /usr/share/nginx/html/app-one-routing


# copy compiled angular sources from build stage
COPY --from=build /usr/angular-build/dist/main-app-event /usr/share/nginx/html

#ENTRYPOINT ["sh", "run.sh"]