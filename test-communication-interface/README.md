# Test-Communication-Interface

## Build and pack library
Use `npm run build` to transpile and also bundle the library to dist (for transpiled files) and lib (for bundled file for vanila JavaScript).
To pack the library use  the `npm pack` command.
Don't bundle rxjs dependency for this lib!
For more information see: https://docs.npmjs.com/cli/v7/configuring-npm/package-json