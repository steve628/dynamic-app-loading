export class EventEmitter<T> {
    private eventCallbacks: Array<(data: T) => void | undefined> = [];

    public on(listener: (data: T) => void ): void {
        this.eventCallbacks.push(listener);
    }

    public removeListener(listener: (data: T) => void): void {
        this.eventCallbacks = this.eventCallbacks.filter(l => l !== listener);
    }

    public emit(data: T): void {
        this.eventCallbacks.forEach(callback => callback(data));
    }
}