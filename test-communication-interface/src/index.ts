export { addDataListener, sendData } from './data-service';
export { Data } from './shared/interfaces/data';