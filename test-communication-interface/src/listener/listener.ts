import { EventEmitter } from '../event-emitter/event-emitter';

export abstract class Listener<T> {
    private eventEmitter = new EventEmitter<T>();

    constructor() {
        this.addListener();
    }
    
    private addListener(): void {
        window.addEventListener('message', this.reciveData.bind(this), false);
    }
    
    private reciveData(message: MessageEvent): void {
        if (message.origin !== window.origin) {
            return;
        }

        if (this.isTypeOf(message.data)) {
            this.eventEmitter.emit(message.data);
        }
    }

    protected abstract isTypeOf(data: any): data is T;
    
    public setCallback(callback: (data: T) => void): void {
        this.eventEmitter.on(callback);
    }
}