import { Data } from '../shared/interfaces/data';
import { Listener } from './listener';

export class DataListener<T extends Data> extends Listener<T> {
    private static dataListener: DataListener<Data>;

    private constructor() {
        super();
    }

    public static getDataListener(): DataListener<Data> {
        if (!DataListener.dataListener) {
            DataListener.dataListener = new DataListener<Data>();
        }

        return DataListener.dataListener;
    }

    protected isTypeOf(data: any): data is T {
        return 'text' in data;
    }
}