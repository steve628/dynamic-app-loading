export interface Data {
    text: string;
    count?: number;
}