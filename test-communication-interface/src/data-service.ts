import { Data } from './shared/interfaces/data';
import { DataListener } from './listener/data-listener';

function addDataListener(callback: (data: Data) => void): void {
    const listener = DataListener.getDataListener();
    listener.setCallback(callback);
}

function sendData(data: Data): void {
    if (window.top) {
        window.top.postMessage(data, window.origin);
    } else {
        throw Error('App is not embedded');
    }
}

export {
    addDataListener,
    sendData
};