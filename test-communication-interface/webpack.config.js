const path = require('path');

module.exports = {
    mode: "production",
    entry: "./src/index.ts", // or './src/index.ts' if TypeScript
    output: {
        filename: "communication.js", // Desired file name. Same as in package.json's "main" field.
        path: path.resolve(__dirname, "lib"),
        library: "communicationService", // Desired name for the global variable when using as a drop-in script-tag.
        libraryTarget: "umd",
        globalObject: "this"
    },
    module: {
        rules: [
            {
                test: /\.ts$|tsx/, // If you are using TypeScript: /\.tsx?$/
                include: path.resolve(__dirname, "src"),
                use: [
                    
                // If _instead_ using ts-loader
                    {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: true
                        }
                    }
                ],
                exclude: /node_modules/
            }
        ]
    },
    // If using TypeScript
    resolve: {
        extensions: ['.tsx', '.ts', '.jsx', 'js']
        },
};