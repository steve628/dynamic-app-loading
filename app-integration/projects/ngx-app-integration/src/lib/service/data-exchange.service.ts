import { Injectable } from '@angular/core';
import { Data, isData } from '../model/data';
import { DataActions } from '../store/data';
import { Store } from '@ngrx/store';

@Injectable({
  providedIn: 'root'
})
export class DataExchangeService {

  constructor(private store: Store) {
    window.addEventListener('message', this.reciveData.bind(this), false);
  }

  public sendData(data: Data): void {
    this.store.dispatch(DataActions.sendData({ data }));
  }

  private reciveData(message: MessageEvent): void {
    if (message.origin !== window.origin) {
      return;
    }
    if (isData(message.data)) {
      const data: Data = message.data;
      this.store.dispatch(DataActions.setData({ data }));
    }
  }
}
