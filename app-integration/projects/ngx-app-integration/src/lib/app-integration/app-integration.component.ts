import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
  AfterViewInit,
  ChangeDetectionStrategy
} from '@angular/core';
import { SafeResourceUrl } from '@angular/platform-browser';
import { Target } from '../model/target';

@Component({
  selector: 'lib-app-integration',
  templateUrl: './app-integration.component.html',
  styleUrls: ['./app-integration.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppIntegrationComponent implements AfterViewInit {
  @ViewChild('myApp') myApp!: ElementRef;
  @Input() app!: SafeResourceUrl;
  @Output() setApp = new EventEmitter<Target>();

  ngAfterViewInit(): void {
    console.log('[NGX] AppObject: ', this.myApp);
    this.setApp.emit(this.myApp.nativeElement);
  }

}
