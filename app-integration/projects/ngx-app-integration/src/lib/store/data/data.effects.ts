import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import * as DataActions from './data.actions';
import * as TargetSelectors from '../target/target.selectors';



@Injectable()
export class DataEffects {

  sendData$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DataActions.sendData),
      concatLatestFrom(() => this.store.select(TargetSelectors.getTargetElement)),
      map(([action, target]) => {
        console.log('[DataEffects] send data to target');
        if (target) {
          console.log('[DataEffects] target to send: ', target);
          target.contentWindow?.postMessage(action.data, window.origin);
        }
      })
    );
  }, { dispatch: false });

  constructor(
    private actions$: Actions,
    private store: Store
  ) { }

}
