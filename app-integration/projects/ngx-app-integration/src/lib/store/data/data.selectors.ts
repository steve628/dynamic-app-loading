import { createFeatureSelector, createSelector } from '@ngrx/store';
import { getModuleFeatureState, ModuleState } from '../module.state';
import { State as DataState } from './data.reducer';

export const getDataFeatureState = createSelector(
  getModuleFeatureState,
  (state: ModuleState) => state.data
);

export const getData = createSelector(
  getDataFeatureState,
  (state: DataState) => {
    console.log('[NGX DataSelectors] state: ', state);
    console.log('[NGX DataSelectors] data: ', state.data);
    return state.data;
  }
);
