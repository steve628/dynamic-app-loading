import { createReducer, on } from '@ngrx/store';
import { Data } from '../../model/data';
import * as DataActions from './data.actions';


export const dataFeatureKey = 'data';

export interface State {
  data: Data;
}

export const initialState: State = {
  data: {
    text: '',
  },
};


export const reducer = createReducer(
  initialState,
  on(DataActions.setData, (state, { data }): State => ({
    ...state,
    data
  })),
);

