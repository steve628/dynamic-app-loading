import { createAction, props } from '@ngrx/store';
import { Data } from '../../model/data';

export const setData = createAction(
  '[Data] Set Data',
  props<{ data: Data }>()
);

export const sendData = createAction(
  '[Data] Send Data',
  props<{ data: Data }>()
);
