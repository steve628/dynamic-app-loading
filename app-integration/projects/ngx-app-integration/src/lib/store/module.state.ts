import {
  Action,
  combineReducers,
  createFeatureSelector,
  MetaReducer
} from "@ngrx/store";

import * as DataFeatures from './data/data.reducer';
import * as TargetFeatures from './target/target.reducer';

export const moduleFeatureKey = 'AppIntegrationModuleStore';

export const getModuleFeatureState = createFeatureSelector<ModuleState>(
  moduleFeatureKey
);

export interface ModuleState {
  [DataFeatures.dataFeatureKey]: DataFeatures.State;
  [TargetFeatures.targetFeatureKey]: TargetFeatures.State;
}

export function reducers(state: ModuleState | undefined, action: Action) {
  return combineReducers({
    [DataFeatures.dataFeatureKey]: DataFeatures.reducer,
    [TargetFeatures.targetFeatureKey]: TargetFeatures.reducer,
  })(state, action);
}

export const metaReducers: MetaReducer<ModuleState>[] = [];
