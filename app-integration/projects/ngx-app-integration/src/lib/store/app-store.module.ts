import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromStore from './module.state';
import { EffectsModule } from '@ngrx/effects';
import { DataEffects } from './data/data.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      fromStore.moduleFeatureKey,
      fromStore.reducers,
      { metaReducers: fromStore.metaReducers }
    ),
    EffectsModule.forFeature([DataEffects]),
  ]
})
export class AppStoreModule { }
