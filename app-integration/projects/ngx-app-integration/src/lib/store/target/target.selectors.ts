import { createFeatureSelector, createSelector } from '@ngrx/store';
import { getModuleFeatureState, ModuleState } from '../module.state';
import { State as TargetState } from './target.reducer';

export const getTargetFeatureState = createSelector(
  getModuleFeatureState,
  (state: ModuleState) => state.target
);

export const getTargetElement = createSelector(
  getTargetFeatureState,
  (state: TargetState) => {
    console.log('[NGX TargetSelectors] state: ', state);
    console.log('[NGX TargetSelectors] target: ', state.target);
    return state.target;
  }
);
