import { createAction, props } from '@ngrx/store';
import { Target } from '../../model/target';

export const setTarget = createAction(
  '[Target] Set Target',
  props<{ target: Target }>()
);
