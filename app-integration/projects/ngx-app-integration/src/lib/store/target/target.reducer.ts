import { createReducer, on } from '@ngrx/store';
import { Target } from '../../model/target';
import * as TargetActions from './target.actions';


export const targetFeatureKey = 'target';

export interface State {
  target: Target | null;
}

export const initialState: State = {
  target: null,
};


export const reducer = createReducer(
  initialState,
  /* on(TargetActions.setTarget, (state, { target }): State => ({
    ...state,
    target
  })), */
  on(TargetActions.setTarget, (state, { target }): State => {
    console.log('[NGX TargetReducer] target: ', target);
    return {
      ...state,
      target,
    };
  }),
);
