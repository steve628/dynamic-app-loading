import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  OnDestroy,
  ChangeDetectionStrategy
} from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Target } from './model/target';
import { Store } from '@ngrx/store';
import { DataSelectors } from './store/data';
import { TargetActions } from './store/target';
import { Data } from './model/data';
import { DataExchangeService } from './service/data-exchange.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ngx-app-integration',
  template: `
    <lib-app-integration *ngIf="app" [app]="app" (setApp)="setTargetElement($event)"></lib-app-integration>
  `,
  styles: [
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NgxAppIntegrationComponent implements OnInit, OnDestroy {
  @Input() set appUrl(val: string) {
    console.log('[NGX] appUrl before checking: ', val);
    if (val) {
      console.log('[NGX] appUrl after checking: ', val);
      this.app = this.sanitizer.bypassSecurityTrustResourceUrl(val);
    }
  }

  public app!: SafeResourceUrl;

  @Input() set sendData(val: Data) {
    console.log('[NGX] data before checking: ', val);
    if (val) {
      console.log('[NGX] data after checking: ', val);
      this.exchange.sendData(val);
    }
  }

  @Output() reciveData = new EventEmitter<Data>();

  private destroy$ = new Subject();

  constructor(
    private sanitizer: DomSanitizer,
    private exchange: DataExchangeService,
    private store: Store,
  ) { }

  ngOnInit(): void {
    console.log('[NGX] Started app integration module');
    this.store
      .select(DataSelectors.getData)
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => this.reciveData.emit(data));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

  setTargetElement(event: Target): void {
    console.log('Set target element to store: ', event);
    this.store.dispatch(TargetActions.setTarget({ target: event }));
  }

}
