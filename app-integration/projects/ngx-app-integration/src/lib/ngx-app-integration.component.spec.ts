import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxAppIntegrationComponent } from './ngx-app-integration.component';

describe('NgxAppIntegrationComponent', () => {
  let component: NgxAppIntegrationComponent;
  let fixture: ComponentFixture<NgxAppIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgxAppIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgxAppIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
