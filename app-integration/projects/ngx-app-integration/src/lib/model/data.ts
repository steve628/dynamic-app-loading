export interface Data {
  text: string;
  count?: number;
}

export function isData(object: any): object is Data {
  return 'text' in object;
}
