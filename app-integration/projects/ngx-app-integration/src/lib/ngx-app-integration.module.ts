import { NgModule } from '@angular/core';
import { NgxAppIntegrationComponent } from './ngx-app-integration.component';
import { AppIntegrationComponent } from './app-integration/app-integration.component';
import { CommonModule } from '@angular/common';
import { AppStoreModule } from './store/app-store.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';



@NgModule({
  declarations: [
    NgxAppIntegrationComponent,
    AppIntegrationComponent
  ],
  imports: [
    CommonModule,
    AppStoreModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      name: 'NGX App Integration Module',
    }),
  ],
  exports: [
    NgxAppIntegrationComponent
  ]
})
export class NgxAppIntegrationModule { }
