/*
 * Public API Surface of ngx-app-integration
 */

export * from './lib/ngx-app-integration.component';
export * from './lib/ngx-app-integration.module';
