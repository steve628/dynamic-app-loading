import { Injectable } from '@angular/core';
import { AppLocation } from '../models/app';

@Injectable({
  providedIn: 'root'
})
export class AppLocationService {
  private readonly _apps: AppLocation[] = [
    {
      name: 'App One',
      value: 'app-one',
    },
    {
      name: 'App Two',
      value: 'app-two',
    },
    {
      name: 'App-Three',
      value: 'app-three',
    },
    {
      name: 'App Unity',
      value: 'http://localhost:4200/unity/',
    },
    {
      name: 'App One Routing',
      value: 'http://localhost:4200/app-one-routing/',
    }
  ];

  public get apps() {
    return this._apps;
  }
}
