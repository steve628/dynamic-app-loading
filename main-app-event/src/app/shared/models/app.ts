export interface AppLocation {
  name: string;
  value: string;
}
