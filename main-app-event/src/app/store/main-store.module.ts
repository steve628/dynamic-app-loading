import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { moduleFeatureKey } from './module.state';
import * as fromStore from './module.state';
import { EffectsModule } from '@ngrx/effects';
import { AppsEffects } from './apps/apps.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      moduleFeatureKey,
      fromStore.reducers,
      { metaReducers: fromStore.metaReducers }
    ),
    EffectsModule.forFeature([AppsEffects]),
  ]
})
export class MainStoreModule { }
