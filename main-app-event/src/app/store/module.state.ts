import {
  Action,
  combineReducers,
  createFeatureSelector,
  MetaReducer
} from '@ngrx/store';

import * as AppsFeatures from './apps/apps.reducer';

export const moduleFeatureKey = 'moduleFeature';

export const getModuleFeatureState = createFeatureSelector<ModuleState>(
  moduleFeatureKey
);

export interface ModuleState {
  [AppsFeatures.appsFeatureKey]: AppsFeatures.State;
}

export function reducers(state: ModuleState | undefined, action: Action) {
  return combineReducers({
    [AppsFeatures.appsFeatureKey]: AppsFeatures.reducer
  })(state, action);
}

export const metaReducers: MetaReducer<MetaReducer>[] = [];
