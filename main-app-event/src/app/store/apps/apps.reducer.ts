import { createReducer, on } from '@ngrx/store';
import * as AppsActions from './apps.actions';
import { HttpErrorResponse } from '@angular/common/http';


export const appsFeatureKey = 'apps';

export interface State {
  app: string;
  loading: boolean;
  error: HttpErrorResponse | null;
}

export const initialState: State = {
  app: '',
  loading: false,
  error: null,
};


export const reducer = createReducer(
  initialState,
  on(AppsActions.loadApp, (state): State => ({
    ...state,
    loading: true,
  })),
  on(AppsActions.loadAppSuccess, (state, { app }): State => ({
    ...state,
    app,
    loading: false,
    error: null,
  })),
  on(AppsActions.loadAppFailure, (state, { error }): State => ({
    ...state,
    error
  }))
);

