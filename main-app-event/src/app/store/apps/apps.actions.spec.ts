import * as fromApps from './apps.actions';

describe('loadAppss', () => {
  it('should return an action', () => {
    expect(fromApps.loadApp({ appName: 'test' }).type).toBe('[Apps] Load App');
  });
});
