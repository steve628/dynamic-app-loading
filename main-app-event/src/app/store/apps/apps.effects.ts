import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as AppsActions from './apps.actions';
import { AppService } from '../../api/services/app.service';
import { concatMap, switchMap, tap, map, mergeAll, catchError } from 'rxjs/operators';
import { ZipService } from '../../api/services/zip.service';
import { of } from 'rxjs';



@Injectable()
export class AppsEffects {

  loadApps$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppsActions.loadApp),
      map(action => action.appName),
      map(appName =>
        appName.includes('http')
        ? AppsActions.loadHttpApp({ appName })
        : AppsActions.loadZipApp({ appName }))
    );
  });

  loadHttpApp$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppsActions.loadHttpApp),
      map(action => action.appName),
      tap(app => console.log('URL of app: ', app)),
      map(app => AppsActions.loadAppSuccess({ app }))
    );
  });

  loadZipApp$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppsActions.loadZipApp),
      tap(() => console.log('[AppsEffects] started loading')),
      switchMap(action =>
        this.appService.downloadZip(action.appName)
          .pipe(
            tap(data => console.log('[AppsEffects] Response data: ', data)),
            switchMap(buffer =>
              this.zipService.getAppResource(buffer)
                .pipe(
                  tap(app => console.log(app)),
                  map(app => AppsActions.loadAppSuccess({ app })),
                  catchError(error => of(AppsActions.loadAppFailure({ error })))
                )
            ),
            catchError(error => of(AppsActions.loadAppFailure({ error })))
          )
      ),
    )
  });

  constructor(
    private actions$: Actions,
    private appService: AppService,
    private zipService: ZipService,
  ) { }

}
