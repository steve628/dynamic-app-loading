import { createSelector } from '@ngrx/store';
import { State as AppsState, appsFeatureKey } from './apps.reducer';
import { getModuleFeatureState, ModuleState } from '../module.state';

export const getAppsFeatureState = createSelector(
  getModuleFeatureState,
  (state: ModuleState) => state.apps
);

export const getCurrentApp = createSelector(
  getAppsFeatureState,
  (state: AppsState) => state.app
);
