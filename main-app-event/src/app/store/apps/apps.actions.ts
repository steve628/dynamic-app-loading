import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';

export const loadApp = createAction(
  '[Apps] Load App',
  props<{ appName: string }>()
);

export const loadAppSuccess = createAction(
  '[Apps] Load App Success',
  props<{ app: string }>()
);

export const loadAppFailure = createAction(
  '[Apps] Load App Failure',
  props<{ error: HttpErrorResponse }>()
);

export const loadZipApp = createAction(
  '[Apps] Load Zip App',
  props<{ appName: string }>()
);

export const loadHttpApp = createAction(
  '[Apps] Load Http App',
  props<{ appName: string }>()
);
