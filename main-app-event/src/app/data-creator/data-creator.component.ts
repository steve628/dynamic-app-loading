import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Data } from 'ngx-app-integration/lib/model/data';

@Component({
  selector: 'app-data-creator',
  templateUrl: './data-creator.component.html',
  styleUrls: ['./data-creator.component.scss']
})
export class DataCreatorComponent implements OnInit {
  dataForm!: FormGroup;
  @Output() sendData = new EventEmitter<Data>();

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.dataForm = this.fb.group({
      text: ['', Validators.required],
      count: ['', Validators.required]
    });
  }

  submitData(): void {
    const data: Data = { ...this.dataForm.value };
    this.sendData.emit(data);
    this.dataForm.reset();
  }

  resetForm(): void {
    this.dataForm.reset();
  }
}
