import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { loadAsync } from 'jszip';




@Injectable({
  providedIn: 'root'
})
export class ZipService {

  constructor() { }

  getAppResource(buffer: ArrayBuffer): Observable<string> {
    return new Observable<string>(observer => {
      loadAsync(buffer)
        .then(zip => {
          zip.file('index.html')?.async('blob')
            .then(uncompressed => {
              const app = new Blob([uncompressed], { type: 'text/html' });
              const appUrl = URL.createObjectURL(app);
              observer.next(appUrl);
              observer.complete();
            })
            .catch(error => observer.error(error))
        })
        .catch(error => observer.error(error))
    });
  }
}
