import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private appRoute = 'http://localhost:4200/apps/';

  private zipHeader = new HttpHeaders({
    'Accept': 'application/zip',
  });

  constructor(
    private http: HttpClient
  ) { }


  public downloadZip(appName: string): Observable<ArrayBuffer> {
    return this.http
      .get(this.appRoute + appName + '.zip', { responseType: 'arraybuffer', headers: this.zipHeader })
      .pipe(
        catchError(error => throwError(error)),
        tap(response => console.log('[AppService] returned response: ', response)),
      );
  }
}
