import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IncludeAppComponent } from './include-app/include-app.component';
import { MaterialModule } from './material/material.module';
import { DataCreatorComponent } from './data-creator/data-creator.component';
import { NgxAppIntegrationModule } from 'ngx-app-integration';
import { HttpClientModule } from '@angular/common/http';
import { MainStoreModule } from './store/main-store.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

@NgModule({
  declarations: [
    AppComponent,
    IncludeAppComponent,
    DataCreatorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    MainStoreModule,
    NgxAppIntegrationModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      name: 'Main App Event',
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
