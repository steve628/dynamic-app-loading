import { ChangeDetectionStrategy, Component, ElementRef, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatRadioChange } from '@angular/material/radio';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { Data } from 'ngx-app-integration/lib/model/data';
import { AppsActions, AppsSelectors } from '../store/apps';
import { Observable } from 'rxjs';
import { AppLocationService } from '../shared/services/app-location.service';

@Component({
  selector: 'app-include-app',
  templateUrl: './include-app.component.html',
  styleUrls: ['./include-app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IncludeAppComponent {

  public app$: Observable<string>;

  myData!: Data;

  currentApp!: string;

  constructor(
    private sanitizer: DomSanitizer,
    private store: Store,
    public appLocationService: AppLocationService,
  ) {
    this.app$ = this.store.select(AppsSelectors.getCurrentApp);
  }

  changeApp(event: MatRadioChange): void {
    this.currentApp = event.value;
    this.store.dispatch(AppsActions.loadApp({ appName: event.value }));
  }

  sendData(event: Data): void {
    this.myData = event;
  }

  reciveData(data: Data): void {
    if (data?.text) {
      alert(data?.text);
    }
  }
}
