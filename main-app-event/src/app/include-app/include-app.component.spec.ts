import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IncludeAppComponent } from './include-app.component';

describe('IncludeAppComponent', () => {
  let component: IncludeAppComponent;
  let fixture: ComponentFixture<IncludeAppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IncludeAppComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IncludeAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
