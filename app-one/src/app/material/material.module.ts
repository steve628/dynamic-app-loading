import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonModule } from '@angular/material/button';

const MATERIALS = [
  ReactiveFormsModule,
  MatInputModule,
  MatFormFieldModule,
  MatRadioModule,
  MatButtonModule,
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MATERIALS,
  ],
  exports: [
    MATERIALS
  ]
})
export class MaterialModule { }
