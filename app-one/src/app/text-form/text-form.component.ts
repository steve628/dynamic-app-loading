import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-text-form',
  templateUrl: './text-form.component.html',
  styleUrls: ['./text-form.component.scss']
})
export class TextFormComponent implements OnInit {
  textForm!: FormGroup;
  @Output() sendText = new EventEmitter<string>();

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.textForm = this.fb.group({
      text: ['', Validators.required]
    });
  }

  onSubmit(): void {
    const text = this.textForm.get('text')?.value as string;
    this.sendText.emit(text);
    this.textForm.reset();
  }

  onReset(): void {
    this.textForm.reset();
  }
}
