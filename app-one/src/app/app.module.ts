import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { DummyComponent } from './dummy/dummy.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { TextFormComponent } from './text-form/text-form.component';
import { DataProcessorComponent } from './data-processor/data-processor.component';
import { AppRoutingModule } from './app-routing.module';
import { RoutingTestComponent } from './routing-test/routing-test.component';

@NgModule({
  declarations: [
    AppComponent,
    DummyComponent,
    TextFormComponent,
    DataProcessorComponent,
    RoutingTestComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
