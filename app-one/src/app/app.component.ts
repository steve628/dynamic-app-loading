import { Component, OnInit } from '@angular/core';
import { addDataListener, sendData, Data } from 'test-communication-interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  private _data!: Data;

  public set data(val: Data) {
    this._data = val;
  }

  public get data() {
    return this._data;
  }

  ngOnInit(): void {
    addDataListener(data => this.data = data);
  }

  onClick(): void {
    sendData({
      text: 'I was clicked!',
      count: 0
    });
  }

  onSendText(event: string): void {
    sendData({
      text: event,
      count: 0
    });
  }
}
