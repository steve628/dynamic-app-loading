import { Component, Input } from '@angular/core';
import { Data } from 'test-communication-interface';

@Component({
  selector: 'app-data-processor',
  templateUrl: './data-processor.component.html',
  styleUrls: ['./data-processor.component.scss']
})
export class DataProcessorComponent {
  @Input() data!: Data;
}
