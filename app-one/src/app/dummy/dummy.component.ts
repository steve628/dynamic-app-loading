import { Component, EventEmitter, Output } from '@angular/core';
import { MatRadioChange } from '@angular/material/radio';

@Component({
  selector: 'app-dummy',
  templateUrl: './dummy.component.html',
  styleUrls: ['./dummy.component.scss']
})
export class DummyComponent {
  readonly flowerImagePath = 'https://dieblumenbringer.de/wp-content/uploads/2018/08/Blumen-im-Abo-Premium-scaled.jpg';
  readonly puppiesImagePath = 'https://i.pinimg.com/originals/82/3d/db/823ddb5c1df3a88f62d5bc0c61a4a392.jpg';

  @Output() sendText = new EventEmitter<string>();
  currentImage = this.flowerImagePath;

  onSelectionChanged(event: MatRadioChange): void {
    this.currentImage = event.value;
  }
}
