const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  mode: 'production',
  entry: [
    path.resolve(__dirname, 'dist/app-one/main.7ee66f997f58e73e2ee8.js'),
    path.resolve(__dirname, 'dist/app-one/polyfills.ea4495fc27642f8e62e9.js'),
    path.resolve(__dirname, 'dist/app-one/runtime.642909cfa3b43a864dbc.js')
  ],
  output: {
    path: path.resolve(__dirname, 'bundled'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader']
      },
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: false,
      cache: false,
      filename: 'index.html',
      template: './dist/app-one/index.html',
      favicon: './dist/app-one/favicon.ico',
    }),
    new MiniCssExtractPlugin({ filename: 'styles.*.css' })
  ]
};
